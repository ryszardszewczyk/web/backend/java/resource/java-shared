#!/usr/bin/env sh
script_dir="$(realpath "$(dirname "$0")")"
mvn versions:display-dependency-updates -Dmaven.version.rules="file://${script_dir}/dependency-rules.xml"
mvn versions:display-property-updates -Dmaven.version.rules="file://${script_dir}/dependency-rules.xml"
mvn versions:display-plugin-updates -Dmaven.version.rules="file://${script_dir}/dependency-rules.xml"

# plugin dependencies are not updated, except for properties
mvn versions:use-latest-releases -Dmaven.version.rules="file://${script_dir}/dependency-rules.xml"
mvn versions:update-properties -Dmaven.version.rules="file://${script_dir}/dependency-rules.xml" -DexcludeProperties=api.version
mvn versions:update-parent -Dmaven.version.rules="file://${script_dir}/dependency-rules.xml"

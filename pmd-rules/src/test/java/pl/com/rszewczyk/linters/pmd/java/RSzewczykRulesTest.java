package pl.com.rszewczyk.linters.pmd.java;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import java.util.Collections;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import net.sourceforge.pmd.test.SimpleAggregatorTst;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@SuppressWarnings("java:S2187")
class RSzewczykRulesTest extends SimpleAggregatorTst {
    private static final String RULESET_RESOURCE_PATH = "pl/com/rszewczyk/linters/pmd/java/pmd-custom.xml";
    private static final List<String> RSZEWCZYK_RULES = Arrays.asList(
            // keep-sorted start
            "RSzewczykBadName",
            "RSzewczykConfigurationPropertiesValidated",
            "RSzewczykInvalidEqualsAndHashCodeInclude",
            "RSzewczykMissingBuilderDefault",
            "RSzewczykMissingConstructorArgNullabilityValidation",
            "RSzewczykMissingFetchType",
            "RSzewczykMissingFieldNullabilityValidation",
            "RSzewczykMissingMethodArgNullabilityValidation",
            "RSzewczykMultipleLocalDateNow",
            "RSzewczykOptionalOrElse",
            "RSzewczykOverriddenMethodArgsWithAnnotations",
            "RSzewczykPublicMethodWithoutTransactional",
            "RSzewczykUseStaticImport"
            // keep-sorted end
    );

    @Override
    protected void setUp() {
        for (String ruleName: RSZEWCZYK_RULES) {
            addRule(
                    RULESET_RESOURCE_PATH,
                    ruleName);
        }
    }

    @Test
    void rszewczykRulesAlphabeticalOrder() {
        List<String> rszewczykRules = getRSzewczykRules();
        List<String> rulesAphabeticalOrder = rszewczykRules.stream().sorted().collect(Collectors.toList());
        assertEquals(rulesAphabeticalOrder, rszewczykRules);
    }

    @Test
    void allRulesHaveTest() {
        List<String> rszewczykRules = getRSzewczykRules();
        Set<String> rszewczykRulesWithTests = new HashSet<>(RSZEWCZYK_RULES);
        Set<String> rszewczykRulesWithoutTests = rszewczykRules.stream().filter(rule -> !rszewczykRulesWithTests.contains(rule)).collect(Collectors.toSet());

        assertEquals(Collections.emptySet(), rszewczykRulesWithoutTests);
    }

    @SuppressFBWarnings("XXE_DOCUMENT")
    private static List<String> getRSzewczykRules() {
        try {
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse("src/main/resources/" + RULESET_RESOURCE_PATH);
            XPath xPath = XPathFactory.newInstance().newXPath();
            String expression = "/ruleset/rule[@name]/@name";
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODESET);

            return IntStream.range(0, nodeList.getLength())
                    .mapToObj(nodeList::item)
                    .map(Node::getNodeValue)
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}

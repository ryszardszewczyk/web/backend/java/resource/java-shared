# Description

Repository contains repetitive configuration for projects written in Java.
To reduce boilerplate copying configuration between projects and problems
with management and updating, this project is aimed to be a git submodule,
that can be included in your own project.

## Usage (submodule)

From your own project:

```shell
git submodule add https://gitlab.com/ryszardszewczyk/web/backend/java/resource/java-shared.git shared
cd shared
make
```

## Usage (as standalone project)

Instruction below assumes, that code is stored in directory `~/Project` (or it's subdirectory).
If that's not the case, paths should be adjusted.

#### Downloading repository

```shell
git clone https://gitlab.com/ryszardszewczyk/web/backend/java/resource/java-shared.git ~/Project
```

#### Git configuration

This code should be added to a file `~/.config/git/config`:

```text
[includeIf "gitdir:~/Project/"]
    path = ~/.config/git/work.config
```

For every repository in `~/Project`, including subdirectories, git
will use configuration from additional file as well.
File `~/.config/git/work.config` is ought to be created with following content:

```text
# vim: ft=gitconfig
[core]
hooksPath = ~/Project/java-shared/git/hooks
```

## Generic pom (parent-pom)

POM configures a list of tools, that can be used to manipulate variables.

List of variables with description is written in a separate file `pom.xml`.

Most of the tools run, when profile `ci` is activated.

```text
# Runs linters without testing
mvn -DskipTests -P ci verify
```

```text
# Runs tests, linters, including sonar scanner
mvn -P ci verify sonar:sonar -Dsonar.projectKey=<project_key>
```

### Building image with application

Building and pushing of images is done with [JIB plugin](https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin)
with calling mvn deploy. Plugin doesn't use docker daemon and doesn't store image for docker to see.

In a case of a need for local testing without pushing to a registry, `mvn -P docker compile` builds an image
with the use of docker.

### Order of tests run

By default, maven runs tests in order of tests found in system's files.
It was changed in parent-pom to random order. More information about
why said change was implemented and how it can be configured can be found
in variable `surefire.runOrder` description in `pom.xml`.

IntelliJ uses its own order of tests execution and ignored maven settings.
To remove bugs of tests failure run by maven, you should add following command.

```shell
mvn -P ci test -Dmaven.surefire.debug="-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=localhost:5005"
```

and configure remote bug removal, which is described here
[IDEA documentation](https://www.jetbrains.com/help/idea/work-with-tests-in-maven.html#debug_maven_goal)

### Profiles

- `ci` - Runs building with all plugins activated to build on GitLab, most importantly runs linters, docker image building and checks code coverage
- `docker` - Configures plugin to use docker daemon for image building. Useful when it is desired to test new image locally. By default `jib` doesn't use docker and image is not stored locally. For profile image is built during code compilation (by default it is `deploy` fase)
- `sonarscanner` - Runs linting using SonarScanner server. Profile is activated when project name is passed.

```shell
mvn -P ci verify sonar:sonar -Dsonar.projectKey=<project_key>
```

- `owasp` - Runs project's dependencies scanning to check for known vulnerabilities through [dependency-check-maven plugin](https://jeremylong.github.io/DependencyCheck/dependency-check-maven/).

### Template migration

Process is described in file [migration.md](migration.md)

## Integration with code editors

Running `mvn -DskipTests -P ci verify` might not be the most pleasant experience and can be slow, so
it's worth configuring IDE, that it can itself run some of the tools directly in program:

### IntelliJ

- [PMD](https://plugins.jetbrains.com/plugin/1137-pmd)

Personal configuration can be added by going through `File -> Settings -> PMD`
Plugin is started by going through `Tools -> Run PMD -> Custom Rules -> <added file>`

- [SpotBugs](https://plugins.jetbrains.com/plugin/14014-spotbugs)

Personal configuration can be added by going through `File -> Settings -> Tools -> SpotBugs`.
The most interesting is tab `Filters`, that can be populated with rules exclusion (most of the time it is a file called `spotbugs-exclude.xml`)

- [SonarLint](https://plugins.jetbrains.com/plugin/7973-sonarlint)

In `File -> Settings -> Tools -> SonarLint` should be added connection to SonarScanner server.

# Migration to parent-pom template

Parent-pom is closely related with [GitLab CI template](https://gitlab.com/ryszardszewczyk/web/backend/java/build-tools/ci-templates/-/blob/develop/java/maven/v1.yml).
It is possible to use both configurations independently, but it's for the best to both migrate to this template and build with GitLab CI.
This document describes only migration to parent-pom.

## Adding template to a project

To main pom.xml file in project following should be.

```xml
<parent>
    <groupId>pl.com.rszewczyk</groupId>
    <artifactId>pom</artifactId>
    <version>(most recent version)</version>
</parent>
```

In a case of projects, that already use SpringBoot it is possible, that
parent-pom is already used, that adds dependencies to this project.
With that it should be deleted, and as an alternative add in `dependencyManegement`

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-dependencies</artifactId>
    <version>${spring.boot.version}</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
```

It's important to note, that import in `dependencyManagement` doesn't configure plugins, so if any projects
use some plugins provided by SpringBoot, they should be configured independently through `<build>`.
Most of the time it's not necessary.

Pom is published in `rszewczyk-shared`, so if project doesn't use that registry, following should be added
in `<repositories>`:

```xml
<repository>
    <id>rszewczyk-shared</id>
    <url>https://gitlab.com/api/v4/projects/56514968/packages/maven</url>
    <snapshots>
        <enabled>false</enabled>
    </snapshots>
</repository>
```

It's worth to identify if variable `${distribution.url}` points to valid repository, that artifacts will be deployed to.
By default, it has value `${releases.url}` that is `https://gitlab.com/api/v4/projects/56514968/packages/maven`

Additionally, to reduce time needed to build, it is worth changing `<updatePolicy>` in repositories to `${snapshots.updatePolicy}`.

## Testing

Before migrating it is worth to check how many tests pass. There is a risk, that after configuration change some
of the test files might be ignored, so after successful migration it is worth to check if number of tests passed
hasn't changed.

### Calculating code coverage

#### Single module project

No additional actions needed.

#### Multimodule project

Because of maven limitations, `jacoco` needs another module to calculate coverage for the whole project, if said projects is a multimodule one.

In main pom.xml following should be added

```xml
<profiles>
    <profile>
        <id>jacoco</id>
        <activation>
            <activeByDefault>true</activeByDefault>
        </activation>
        <modules>
            <module>ci</module>
        </modules>
    </profile>
</profiles>
```

and empty ci module, that will be dependent on every other module in a project.

Worth to remember to not add it to `<modules>` in main `pom.xml` file.

Module has also set special properties.

```xml
<properties>
    <jacoco.skip-aggregate>false</jacoco.skip-aggregate>
</properties>
```

It is possible that per module you would need to set `<jacoco.covered.minimum>%n</jacoco.covered.minimum>`,
because code coverage check is still run on single module level.

## Cleaning duplicated configuration from a project.

Added pom contains definitions and configurations for commonly used plugins and tools,
so they can be removed from the project.

## Building images with JIB

Example configuration in `pom.xml` with server can look like this:

```xml
<properties>
    <jib.skip>false</jib.skip>
</properties>
<build>
    <plugins>
        <plugin>
            <groupId>com.google.cloud.tools</groupId>
            <artifactId>jib-maven-plugin</artifactId>
            <executions>
                <execution>
                    <phase>${jib.phase}</phase>
                    <goals>
                        <goal>${jib.goal}</goal>
                    </goals>
                </execution>
            </executions>
            <configuration>
                <skip>${jib.skip}</skip>
                <from>
                    <image>${docker.img.java}</image>
                </from>
                <to>
                    <image>${docker.registry}/nazwa-projektu/${project.artifactId}:${project.version}</image>
                </to>
                <container>
                    <labels>
                        <pl.com.rszewczyk.project>[nazwa projektu]</pl.com.rszewczyk.project>
                        <pl.com.rszewczyk.service>[nazwa usługi]</pl.com.rszewczyk.service>
                    </labels>
                </container>
            </configuration>
        </plugin>
    </plugins>
</build>
```

## Linters configuration

This repository contains basic configuration for [PMD](pmd-custom.xml) and [SpotBugs](spotbugs-exclude.xml).
They can be copied to a project or built from scratch.

Keeping in mind, that terminal is opened in migrated repository directory:

```shell
JAVA_SHARED="<path to java-shared repository>"
cp "${JAVA_SHARED}/pmd-custom.xml" .
cp "${JAVA_SHARED}/spotbugs-exclude.xml" .
```

If a project uses lombok, settings for code generation should be updated in a file `lombok.config`
to avoid excessive number of reported problems:

```lombok.config
lombok.addLombokGeneratedAnnotation=true
lombok.extern.findbugs.addSuppressFBWarnings=true
config.stopBubbling=true
```

## Problem corrections detected by linters

Checking code is called by executing given command `mvn -DskipTests -P ci verify`.
There is also possibility to install `PMD` and `SpotBugs` plugins and run code analysis using them after prior configuration.

If SonarScanner is also meant to be run, there `sonar:sonar -Dsonar.projectKey=<project_key>` should be added.

There is also possibility to lint with Qodana with CI.

## Creating .mvn directory with properties

CI uses `.mvn/settings.xml` file to authorize and storing artifacts.

```xml
<settings
  xmlns="http://maven.apache.org/SETTINGS/1.1.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
    <servers>
        <server>
            <id>rszewczyk-common</id>
            <configuration>
                <httpHeaders>
                    <property>
                        <name>Job-Token</name>
                        <value>${CI_JOB_TOKEN}</value>
                    </property>
                </httpHeaders>
            </configuration>
        </server>
    </servers>
</settings>
```

## Updates of the version

## Checking if a number of run tests is the same as before

## GitLab configuration

In project's settings `Settings -> Merge Requests` there should be a change from `Merge Method` to `Fast-Forward Merge`.
Script [configure_badges.py](./gitlab/configure_badges.py) allows adding badges to a project.

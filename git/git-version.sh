#!/usr/bin/env bash
set -uo pipefail
IFS=$'\n\t'

tag=$(git describe --tags --abbrev=0 2>/dev/null)
# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
	echo "0.0.$(git rev-list --all --count)"
else
	echo "${tag}.$(git rev-list "${tag}.." --count)"
fi
